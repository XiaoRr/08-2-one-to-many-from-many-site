package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

// TODO
//
// 请定义 ParentEntity。其中 ParentEntity 不引用 ChildEntity。并且其对应的数据表定义应当满足如下
// 的条件：
//
// parent_entity
// +─────────+──────────────+──────────────────────────────+
// | Column  | Type         | Additional                   |
// +─────────+──────────────+──────────────────────────────+
// | id      | bigint       | primary key, auto_increment  |
// | name    | varchar(20)  | not null                     |
// +─────────+──────────────+──────────────────────────────+
//
// <--start-
public class ParentEntity {
}
// --end-->

